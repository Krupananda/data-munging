#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 15 13:05:18 2019
@author: krupa
"""
from analyzer import DataAnalyzer
from analyzer import DataExtractor

class weather(object):

    def __init__(self, file_path):

        self.extractor = DataExtractor(file_path)
        self.analyzer = DataAnalyzer()

    def min_temperature_difference(self):
        result = self.extractor.extract_data(0, 1, 2)
        return self.analyzer.analyze_data(result)

if __name__ == "__main__":
    weather_report = weather('weather.dat')
    print(weather_report.min_temperature_difference())
