#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 15 14:30:58 2019

@author: krupa
"""
from analyzer import DataExtractor 
from analyzer import DataAnalyzer

class football(object):

    def __init__(self,file_path):
        self.extractor=DataExtractor(file_path)
        self.analyzer=DataAnalyzer()
#   
    def min_score_difference(self):
         result=self.extractor.extract_data(1,6,8)
         return self.analyzer.analyze_data(result)
        
if __name__=="__main__":
    football_data=football('football.dat')
    print(football_data.min_score_difference())
        