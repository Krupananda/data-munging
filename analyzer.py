#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 15 17:06:56 2019

@author: krupa
"""


class DataExtractor(object):
    

    def __init__(self, path):
        self.path = path
        self.file_tracker = []

    def extract_data(self, name, min_val, max_val):
        with open(self.path, 'r') as f:
            for data in f.readlines():
                self.file_tracker.append(data.split())
        self.file_tracker.pop(0)
        def data_report():
            data_tracker = []
            for data in self.file_tracker:
                if len(data) == 1 or len(data) == 0:
                    continue
                else:
                    print(data[name], data[min_val], data[max_val])
                    data_tracker.append(
                        [data[name], data[min_val], data[max_val]])
            return data_tracker
        return data_report()


class DataAnalyzer(object):

    def analyze_data(self, data):
        minval = None
        name = ""
        for temp in data:
            value_diff = abs(float(temp[1].strip('*')) - float(temp[2].strip('*')))
            if minval == None or minval > value_diff:
                minval = value_diff
                name = temp[0]
        return name, minval
